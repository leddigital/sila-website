<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\ContentsImages;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function index(Request $request) {
        //Pega o tipo de conteudo que vou gerenciar
        $_type = $request->route('_type');
        return view('admin.content.' . $_type . '.index', ['_type' => $_type]);
    }

    //Listar todas as categorias
    public function readAll(Request $request) {
        $type = $request->route('_type');
        $collection = $this->model->where('type', '=', $type)->get()->all();

        $data['data'] = $collection;
        echo json_encode($data);
    }

    public function form(Request $request) {

        $id = $request->route('id');
        $_type = $request->route('_type');

        if (isset($id) and ($id != "")) {

            $entity = $this->model->with(['categories'])->find($id);
            $gallery = $entity->images()->get();

            $categories = Categories::with(['contents' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('title', 'ASC')
                ->get();

            return view('admin.content.' . $_type . '.form', [
                'entity' => $entity,
                'categories' => $categories,
                'gallery' => $gallery,
                '_type' => 'produto',
            ]);

        } else {

            $categories = Categories::orderBy('title', 'ASC')->get();
            return view('admin.content.' . $_type . '.form', ['categories' => $categories, '_type' => $_type]);
        }
    }

    public function save(Request $request) {

        $folder = public_path() . '/content/';
        $form = $request->all();
        $id = $request->route('id');

        // Verifica se a pasta de conteudo já existe, se nao existir, a cria.
        if (!file_exists($folder)) {
            mkdir($folder, 0777);
        }

        // Inserir novo registro
        if (!isset($id) and $id == "") {

            //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model);

            $form['image'] = $this->saveImg($form['base64'], 'produto_', '/produtos/');
            $form['image_alta'] = $this->saveImg($form['base64_2'], 'alta_', '/produtos/alta/');

            $category = new Categories();

            // Fazer inserção do produto

            $entity = $this->model->create($form);

            if (isset($form['categories']) > 0){
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            if ($entity) {

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            //Atualizar o registro
            $entity = $this->model->find($id);

            //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model, $id);

            if ($entity->update($form)) {
                if(isset($form['base64']) && $form['base64']!="") {
                    $update['image'] = $this->saveImg($form['base64'], 'produto_', '/produtos/', $entity->image);
                    $update['image_alta'] = $this->saveImg($form['base64_2'], 'alta_', '/produtos/alta/', $entity->image_alta);
                    $entity->update($update);
                }
            }

            if (isset($form['categories']) > 0) {
                //Remove todas as categorias e adiciona novamente
                $entity->categories()->detach();
                //Adiciona novamente as categorias
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            $res = [
                'status' => 200,
                'data' => $entity,
            ];
        }

        return response()->json($res);
    }

    public function delete(Request $request) {

        $id = $request->route('id');
        $entity = $this->model->find($id);

        // Excluir categorias
        $categories = $entity->categories()->detach();

        if ($entity->delete()) {
            @unlink(public_path() . '/produtos/' . $entity->image);
            @unlink(public_path() . '/produtos/alta/' . $entity->image_alta);
        }
    }
}
