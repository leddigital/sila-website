@extends('layouts.default')
@section('content')


<!-- Content Scroll -->
<div id="content-scroll">


    <!-- Main -->
    <div id="main">

        <!-- Hero Section -->
        <div id="hero">
            <div id="hero-styles" class="parallax-onscroll">
                <div id="hero-caption">
                    <div class="inner">
                        <div class="hero-title">SOBRE A SILA DECOR</div>
                        <div class="hero-subtitle"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--/Hero Section -->


        <!-- Main Content -->
        <div id="main-content">
            <!-- Main Page Content -->
            <div id="main-page-content">



                <!-- Row -->
                <div class="vc_row row_padding_top">

                    <hr>
                    <h3 class="has-mask" data-delay="10">{{ $informations->texto_1 }}</h3>

                </div>
                <!--/Row -->


                <!-- Row -->
                <div class="vc_row row_padding_top row_padding_bottom">
                    <hr>
                    <dl class="accordion has-animation">
                        <dt>
                            <span>{{ $informations->valor_1 }}</span>
                            <div class="acc-icon-wrap parallax-wrap">
                                <div class="acc-button-icon parallax-element">
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </dt>
                        <dd class="accordion-content">{{ $informations->valor_texto_1 }}</dd>

                        <dt>
                            <span>{{ $informations->valor_2 }}</span>
                            <div class="acc-icon-wrap parallax-wrap">
                                <div class="acc-button-icon parallax-element">
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </dt>
                        <dd class="accordion-content">{{ $informations->valor_texto_2 }}</dd>

                        <dt>
                            <span>{{ $informations->valor_3 }}</span>
                            <div class="acc-icon-wrap parallax-wrap">
                                <div class="acc-button-icon parallax-element">
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </dt>
                        <dd class="accordion-content">{{ $informations->valor_texto_3 }}</dd>

                        <dt>
                            <span>{{ $informations->valor_4 }}</span>
                            <div class="acc-icon-wrap parallax-wrap">
                                <div class="acc-button-icon parallax-element">
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </dt>
                        <dd class="accordion-content">{{ $informations->valor_texto_4 }}</dd>

                        <dt>
                            <span>{{ $informations->valor_5 }}</span>
                            <div class="acc-icon-wrap parallax-wrap">
                                <div class="acc-button-icon parallax-element">
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </dt>
                        <dd class="accordion-content">{{ $informations->valor_texto_5 }}</dd>

                    </dl>

                </div>
                <!--/Row -->


                <!-- Row -->
                <div class="vc_row">

                    <h1 class="has-animation" data-delay="100">Nossos Produtos</h1>

                    <hr>
                    <hr>

                    <!-- Clients Table -->
                    <ul class="clients-table has-animation" data-delay="10">
                           @foreach ($categories as $category)
                        <li class="link has-animation" data-delay="150">
                            <div style="display:block; position:relative; height:100%">
                                <div style="position:relative; top:30%;text-transform:uppercase;">
                                    <a class="product-pers-title hide-ball next-ajax-link-page" href="{{ route('nav.categoria', ['categoria' => $category->url]) }}">{{ $category->title }}</a>
                                </div>
                            </div>
                        </li>
                         @endforeach
                        
                    </ul>
                    <!--/Clients Table -->

                </div>
                <!--/Row -->


                <!-- Row -->
                <div class="vc_row row_padding_top">

                    <h3 class="has-mask" data-delay="10">{{ $informations->texto_2 }}</h3>
                    <hr>
                    <hr>

                </div>
                <!--/Row -->

            </div>
            <!--/Main Page Content -->

            <!-- Project Navigation -->
            <div id="page-nav">
                <div class="next-page-wrap">
                    <div class="next-page-title">
                        <div class="inner">
                            <div class="subtitle-info has-animation">Vamos conversar</div>
                            <div class="has-animation" data-delay="150"><a
                                    class="page-title hide-ball next-ajax-link-page" data-type="page-transition"
                                    href="{{ route('nav.contato') }}">CONTATO</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Project Navigation -->


        </div>
        <!--/Main Content -->
    </div>
    <!--/Main -->

    <!-- Footer -->
    <footer class="hidden">
        <div id="footer-container">

            <div id="backtotop" class="button-wrap left-custom">
                <div class="icon-wrap parallax-wrap">
                    <div class="button-icon parallax-element">
                        <i class="fa fa-angle-up"></i>
                    </div>
                </div>
                <div class="button-text"><span data-hover="Voltar ao topo">Voltar ao topo</span></div>
            </div>


            <div class="socials-wrap">
                <div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
                <div class="socials-text">Siga a Sila Decor</div>
                <ul class="socials">
                    <li style="{{ isset($informations->facebook)!=""?'':'display:none;' }}" }}>
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->facebook }}" target="_blank">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->instagram)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->instagram }}" target="_blank">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->linkedin)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->linkedin }}" target="_blank">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>

                    <li style="{{ isset($informations->twitter)!=""?'':'display:none;' }}"><span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->twitter }}" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a></span>
                    </li>

                    <li style="{{ isset($informations->pinterest)!=""?'':'display:none;' }}">
                        <span class="parallax-wrap">
                            <a class="parallax-element" href="{{ $informations->pinterest }}" target="_blank">
                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                            </a>
                        </span>
                    </li>
                </ul>
            </div>

        </div>
    </footer>
    <!--/Footer -->

</div>
<!--/Content Scroll -->

@endsection
