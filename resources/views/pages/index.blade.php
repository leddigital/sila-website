@extends('layouts.default')
@section('content')


<!-- Content Scroll -->
<div id="content-scroll">

    <!-- Main -->
    <div id="main">
        <!-- Main Content -->
        <div id="main-content">

            <!-- Showcase Holder -->
            <div id="showcase-holder">
                <div id="showcase-tilt-wrap">
                    <div id="showcase-tilt">
                        <div id="showcase-slider" class="swiper-container">
                            <div class="swiper-wrapper">

                                @foreach ($banners as $category)

                                    <!-- Section Slide -->
                                    <div class="swiper-slide" data-title="{{ $category->title }}" data-subtitle=""
                                        data-number="01">
                                        <div class="img-mask">
                                            <div class="section-image"
                                                data-src="{{ asset('banners/'.$category->banners) }}"></div>
                                        </div>
                                        <a class="title hide-ball" data-type="page-transition"
                                            href="{{ route('nav.categoria', ['categoria' => $category->url]) }}">{{ $category->title }}<span>02</span></a>
                                    </div>
                                    <!--/Section Slide -->


                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

                <div class="showcase-captions-wrap no-stroked">
                    <div class="showcase-captions"></div>
                </div>
            </div>
            <!-- Showcase Holder -->
        </div>
        <!--/Main Content -->
    </div>
    <!--/Main -->

</div>
<!--/Content Scroll -->

<!-- Footer -->
<footer class="fixed">
    <div id="footer-container">

        <div class="arrows-wrap">
            <div class="prev-wrap parallax-wrap">
                <div class="swiper-button-prev swiper-button-white parallax-element"></div>
            </div>
            <div class="next-wrap parallax-wrap">
                <div class="swiper-button-next swiper-button-white parallax-element"></div>
            </div>
        </div>

        <div class="footer-middle">
            <div class="showcase-subtitles-wrap"></div>
        </div>

        <div class="socials-wrap">
            <div class="socials-icon"><i class="fa fa-share-alt" aria-hidden="true"></i></div>
            <div class="socials-text">Siga a Sila Decor</div>
            <ul class="socials">


                <li style="{{ isset($informations->facebook)!=""?'':'display:none;' }}" }}>
                    <span class="parallax-wrap">
                        <a class="parallax-element" href="{{ $informations->facebook }}" target="_blank">
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                        </a>
                    </span>
                </li>

                <li style="{{ isset($informations->instagram)!=""?'':'display:none;' }}">
                    <span class="parallax-wrap">
                        <a class="parallax-element" href="{{ $informations->instagram }}" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </span>
                </li>

                <li style="{{ isset($informations->linkedin)!=""?'':'display:none;' }}">
                    <span class="parallax-wrap">
                        <a class="parallax-element" href="{{ $informations->linkedin }}" target="_blank">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    </span>
                </li>

                <li style="{{ isset($informations->twitter)!=""?'':'display:none;' }}"><span class="parallax-wrap">
                        <a class="parallax-element" href="{{ $informations->twitter }}" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a></span>
                </li>

                <li style="{{ isset($informations->pinterest)!=""?'':'display:none;' }}">
                    <span class="parallax-wrap">
                        <a class="parallax-element" href="{{ $informations->pinterest }}" target="_blank">
                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                        </a>
                    </span>
                </li>
            </ul>
        </div>

    </div>
</footer>
<!--/Footer -->


@endsection
