$(document).ready(function(){

    (function($) {
        "use strict";

    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                comments: {
                    required: true,
                    minlength: 20
                }
            },
            messages: {
                name: {
                    required: "Por favor informe seu nome",
                    minlength: "Seu nome precisa conter ao menos 2 caracteres"
                },
                email: {
                    required: "Por favor informe seu e-mail"
                },
                comments: {
                    required: "Sua mensagem não pode ser vazia",
                    minlength: "Sua mensagem não pode ter menos de 20 caracteres"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    type:"POST",
                    // data: $(form).serialize(),
                    url: $(form).attr('action'),
                    beforeSend: function(data) {
                        $("#contactForm :input").prop("disabled", true);
                    },
                    success: function(data) {
                        ohSnap('Mensagem enviada com sucesso.', {'duration':'2000'});
                    },
                    error: function(data) {
                        ohSnap('Falha ao enviar mensagem', {color: 'red'});
                    }
                })
            }
        })
    })

 })(jQuery)
})
